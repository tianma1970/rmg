
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/">Rent My Gear</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link" href="/">Hem</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/locations">Sök</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/articles">Artiklar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/">Logga in/registrera</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/">Mina sidor</a>
                </li>

            </ul>
        </div>
    </div>
</nav>
